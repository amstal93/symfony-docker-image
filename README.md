# Symfony Docker Base

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/brpaz/symfony-docker-image.svg?style=for-the-badge)
![Docker Pulls](https://img.shields.io/docker/pulls/brpaz/symfony.svg?style=for-the-badge)
![MicroBadger Layers](https://img.shields.io/microbadger/layers/brpaz/symfony.svg?style=for-the-badge)
![MicroBadger Size](https://img.shields.io/microbadger/image-size/brpaz/symfony.svg?style=for-the-badge)

> An all in one base Docker image for Symfony Applications containing NGINX and PHP 7.3. PaaS Friendly.

## Description

This image provides all the basics to dockerize a [Symfony](https://symfony.com) Application.

Its optimized for running Symfony on PaaS systems like [Heroku Docker Runtime](https://devcenter.heroku.com/articles/container-registry-and-runtime) or 
[Google Cloud Run](https://cloud.google.com/run/) as it includes Nginx and PHP in the same image, plus allowing to specify the "PORT" environment variable as required by the mentioned PaaS.

## What is included

* PHP 7.3 and all the required extensions by Symfony, plus some other popular extensions like pdo_mysql.
* XDebug enabled in development
* Blackfire Agent
* Composer
* Nginx

## Usage

Add the following to the top of your Dockerfile.

```Dockerfile
FROM brpaz/symfony:latest
```

This image contains "ONBUILD" commands that will automatically install your application dependencies using Composer. 

For this to work, your application Dockerfile should be in the root of your Symfony project.

## Contributing

Contributions, issues and Features requests are welcome.

## Show your support

<a href="https://www.buymeacoffee.com/Z1Bu6asGV" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A Coffee" style="height: 41px !important;width: 174px !important;box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;" ></a>

## License 

Copywright @ 2019 [Bruno Paz](https://github.com/brpaz)

This project is [MIT](LLICENSE) Licensed.
